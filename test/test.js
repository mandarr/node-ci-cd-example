var request = require("supertest");
var app = require("../index.js");
describe("GET /version", function () {
    it("respond with version", function (done) {
        request(app).get("/version").expect('{ success: true, version: "1.0" }', done);
    });
});
