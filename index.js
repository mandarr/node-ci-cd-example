const express = require("express");
const app = express();

app.get("/", async (req, res) => {
    res.status(200).json({ success: true, message: "Docker-Nodejs" });
});

app.get("/version", async (req, res) => {
    res.status(200).json({ success: true, version: "1.0" });
});

app.listen(3000, () => {
    console.log("Server is running on http://localhost:3000");
});

module.exports = app;
